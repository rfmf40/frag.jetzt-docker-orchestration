# FOLLOWING VALUES MUST BE SUBSTITUTED: FQDN, DEEPL_AUTH_KEY

map $http_upgrade $connection_upgrade {
  default upgrade;
  ''      close;
}

server {
  listen 80 default_server;
  listen [::]:80 default_server;
  server_name _;

  return 444;
}

server {
  listen 80;
  listen [::]:80;
  server_name :FQDN;

  return 301 https://$host$request_uri;
}

server {
  listen 443 ssl;
  listen [::]:443 ssl;
  server_name :FQDN;

  ssl_certificate /etc/ssl/certs/public-ssl.pem;
  ssl_certificate_key /etc/ssl/private/private-ssl.pem;

  ssl_session_cache shared:SSL:10m;
  ssl_session_timeout 10m;
  ssl_protocols TLSv1.2;
  ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
  ssl_prefer_server_ciphers on;

  ssl_dhparam /etc/ssl/dhparam-4096.pem;

  root /var/www/frag.jetzt;

  index index.html;

  location / {
    gzip on;
    gzip_types application/javascript application/json text/css image/svg+xml;
    gzip_vary on;
  }

  location ~ ^/(creator|participant|home|imprint|user|moderator).*$ {
    gzip on;
    gzip_types application/javascript application/json text/css image/svg+xml;
    gzip_vary on;
    alias /var/www/frag.jetzt/;
    try_files $uri $uri/ /index.html;
  }

  location ^~ /spacy {
    # proxy_pass http://frag-jetzt-spacy;
    proxy_pass https://spacy.frag.jetzt;
  }

  location ^~ /languagetool {
    rewrite ^/languagetool(.*) /v2/check$1 break;

    # proxy_pass http://frag-jetzt-languagetool:8010;
    proxy_pass https://lt.frag.jetzt;
  }

  location ^~ /deepl {
    rewrite ^/deepl(.*) /v2$1 break;

    proxy_pass https://api-free.deepl.com;

    ##########################
    # SET YOUR AUTH KEY HERE #
    ##########################
    proxy_set_header "Authorization" "DeepL-Auth-Key :DEEPL_AUTH_KEY";
  }

  location ^~ /antworte-jetzt {
    return 307 https://antworte.jetzt;
  }

  location ^~ /api/roomsubscription {
    rewrite ^/api(.*) $1 break;

    proxy_pass http://fragjetzt-ws-gateway:8080;

    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }

  location ^~ /api/ws/websocket {
    rewrite ^/api(.*) $1 break;

    proxy_pass http://fragjetzt-ws-gateway:8080;
    proxy_http_version 1.1;

    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Real-IP $remote_addr;
  }

  location /api {
    rewrite ^/api(.*) $1 break;

    proxy_pass http://fragjetzt-backend:8888;

    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }

  location = /favicon.ico {
    alias /var/www/frag.jetzt/assets/icons/favicon.ico;
  }

}
